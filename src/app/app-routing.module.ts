import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApidocsComponent } from './components/apidocs/apidocs.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { PersonajesComponent } from './components/personajes/personajes.component';
import { RegistroProComponent } from './components/registro-pro/registro-pro.component';


const routes: Routes = [
  {path: 'inicio',component: InicioComponent},
  {path: 'personajes', component: PersonajesComponent},
  {path: 'registroPro', component: RegistroProComponent},
  {path: 'apidocs', component: ApidocsComponent} 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
