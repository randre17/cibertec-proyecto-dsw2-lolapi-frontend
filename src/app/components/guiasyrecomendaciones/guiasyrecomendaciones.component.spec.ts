import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiasyrecomendacionesComponent } from './guiasyrecomendaciones.component';

describe('GuiasyrecomendacionesComponent', () => {
  let component: GuiasyrecomendacionesComponent;
  let fixture: ComponentFixture<GuiasyrecomendacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuiasyrecomendacionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuiasyrecomendacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
